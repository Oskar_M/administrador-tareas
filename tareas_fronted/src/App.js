import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';

import Navigation  from './components/Navigation';
import TaskList  from './components/TaskList';
import CreateTask  from './components/CreateTask';
import CreateUser  from './components/CreateUsers';


function App() {
  return (
   <Router>
     <Navigation/>
     <Route path="/" exact component={TaskList}/>
     <Route path="/edit/:id" component={CreateTask}/>
     <Route path="/create" component={CreateTask}/>
     <Route path="/user" component={CreateUser}/>


     
   </Router>
  );
}

export default App;
