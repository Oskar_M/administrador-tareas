const userCtrl = {};

const User = require('../models/User');

userCtrl.getUsers = async(req, res) => {
    const user = await User.find();
    res.json(user)
}

userCtrl.createUser = async(req, res) => {
    const{username}=req.body; //se extrae el nombre del usuario
    const newUser =new User({username: username}) //se crea un nuevo usuario
    await newUser.save(); //se guarda el nuevo usuario
    res.json('Usuario Creado')
}

userCtrl.deleteUser = async(req, res) => {
    await User.findByIdAndDelete(req.params.id)
    res.json('Usuario Borrado')
}


module.exports = userCtrl;