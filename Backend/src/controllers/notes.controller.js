const notesCtrl = {};

const Note = require('../models/Note');

//devuelve muchas tareas
notesCtrl.getTasks = async(req, res) => {
    const notes = await Note.find();//find consulta todos las tareas en la base de datos y los devuelve como un arreglo
    res.json(notes)
}
//crea una tarea 
notesCtrl.createTask = async (req, res) => {
    const {title, description, date, author} = req.body;
    const newNote =new Note({
        title:  title,
        description: description,
        date: date,
        author: author
    })
    await newNote.save()
    res.json({message: 'Tarea Guardada'})
}
//obtiene una tarea
notesCtrl.getTask = async(req, res) => {
    const note =await Note.findById(req.params.id);
    res.json(note)
}
//actualiza una tarea
notesCtrl.updateTask = async(req, res) => {
    const {title, description, author} = req.body;
    await Note.findByIdAndUpdate(req.params.id, {
        title,
        description,
        author

    });
    res.json({message: 'Tarea Actualizada'})
}
//borra una tarea
notesCtrl.deleteTask = async(req, res) => {
    await Note.findByIdAndDelete(req.params.id);
    res.json({message: 'Tarea Borrada'})
}



module.exports = notesCtrl;