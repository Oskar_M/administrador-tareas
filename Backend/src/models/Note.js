//Schema es lo que quiero guardar de los datos o como sean (si unicos, string o enteros)
//modelo de datos es como MongoDB va a estar consultando esos datos
const {Schema, model} =require('mongoose')

const noteSchema = new Schema({
    title: String,
    description: {
        type: String,
        required: true
    },
    author: String,
    date: {
        type: Date,
        default: Date.now
    }

}, {
    timestamps: true
});

 module.exports = model('Note', noteSchema);