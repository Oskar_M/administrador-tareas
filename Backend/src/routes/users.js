const {Router} = require('express');
const { route } = require('./notes');
const router =  Router();

const {getUsers, createUser, deleteUser}=require('../controllers/users.controllers');

router.route('/')
    .get(getUsers) //.get obtiene los datos
    .post(createUser)

router.route('/:id')    
    .delete(deleteUser)

module.exports = router;