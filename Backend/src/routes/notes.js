const {Router} = require('express');
const router =  Router();

const { getTasks, createTask, getTask, updateTask, deleteTask } =require('../controllers/notes.controller');

router.route('/')
    .get(getTasks)
    .post(createTask); //.post sirve para guardar los datos

router.route('/:id') //se agrega id para obtener un dato espefico
    .get(getTask) 
    .put(updateTask) //.put sirve para actualiar los datos
    .delete(deleteTask) //. borra los datos

module.exports = router;