require('dotenv').config(); //importa las variables de entorno en el archivo .env

const app = require('./app');
require('./database');

//Está función se encargará de iniciar el servidor
 async function main(){
    await app.listen(app.get('port'));
    console.log('Server on port,', app.get('port'));
}

main();
