const mongoose = require('mongoose');


//process es un objeto que tiene accesso al sistema operativo
const URI = process.env.MONGODB_URI 
    ? process.env.MONGODB_URI 
    : 'mongodb://localhost/tareas';

mongoose.connect(URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology:true
});

const connection = mongoose.connection;

connection.once('open', ()=>{
    console.log('DB is connected')
});